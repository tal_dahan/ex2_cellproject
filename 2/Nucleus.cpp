#include<string>
#include <iostream>
#include "Nucleus.h"

using namespace std;

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*Function get dna sequence and build a complementary dna 
input:dna sequence
output:non
*/
void Nucleus::init(const std::string dna_sequence)
{
	_DNA_strand = dna_sequence;
	string complementary = "";
	string ch = "";
	int  i = 0;

	//go over the dna sequence and build complementary DNA sequence
	for (i; i < _DNA_strand.length(); i++)
	{
		ch = _DNA_strand[i];
		if (ch == "A")
		{
			complementary = complementary + "T";
		}
		else if (ch == "T")
		{
			complementary = complementary + "A";
		}
		else if (ch == "G")
		{
			complementary = complementary + "C";
		}
		else if (ch == "C")
		{
			complementary = complementary + "G";
		}
		/*If the char didn't mach any of of the attempt so the sequence isn't valid*/
		else
		{
			// writes to cerr - a stream dedicated to error audit
			std::cerr << "problem" << endl;
			//exit 
			_exit(1);
		}
	}
	
	_complementary_DNA_strand =  complementary;

}
/*Function creat RNA 
input:gene
output:RNA for the gene
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene)
{
	int i = 0;
	
	string std = "";
	string rna = "";
	string ch = "";
	/*cheak if the gene is in the complementary strand or not*/
	if (gene.is_on_complementary_dna_strand())
	{
		/*in this case we want creat a RNA to the complementary dna strand*/
		std = _complementary_DNA_strand.substr(gene.get_start(),gene.get_end());
	}
	else
	{
		/*in the case we want to creat a RNA to the original DNA strand*/
		std = _DNA_strand.substr(gene.get_start(), gene.get_end());
	}
	/*creat the RNA*/
	for (i; i < std.length(); i++)
	{
		/*save the current char*/
		ch = std[i];
		/*cheak if the char is "T" to know if w need to replace it with "u"*/
		if (ch  == "T")
		{
			rna = rna + "U";
		}
		else
		{
			rna = rna + ch;
		}
	}

	/*return the RNA*/
	return rna;
}
/*Function return revers dna strand
input:non
output:revers dna strand
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	//save the dna strand value
	string ReversDNA = this->_DNA_strand;
	//revers the string
	reverse(ReversDNA.begin(), ReversDNA.end());
	//return the revers dna strand
	return ReversDNA;
}

/*
Function count how many time codon appear in _DNA_strand
input:codon
output:times codon in  appear in _DNA_strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int M = codon.length();
	int N = this->_DNA_strand.length();
	unsigned int res = 0;

	/* A loop to slide pat[] one by one */
	for (int i = 0; i <= N - M; i++)
	{
		/* For current index i, check for
		   pattern match */
		int j;
		for (j = 0; j < M; j++)
			if (this->_DNA_strand[i + j] != codon[j])
				break;

		// if _DNA_strand[0...M-1] = codon[i, i+1, ...i+M-1] 
		if (j == M)
		{
			res++;
			j = 0;
		}
	}
	//retrun the result
	return res;
}



unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

/////////*setters*//////////////

void Gene::set_start(unsigned int start) 
{
	this->_start = start;
}

void Gene::set_end(unsigned int end) 
{
	this->_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand =  on_complementary_dna_strand;
}

