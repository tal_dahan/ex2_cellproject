#pragma once
#include <iostream>
#include <string>

using std::string;

class Gene
{

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;


public:
	/*functions in the class*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	//getters
	bool is_on_complementary_dna_strand() const;
	unsigned int get_end() const;
	unsigned int get_start() const;
	//setters
	void set_start(unsigned int start) ;
	void set_end(unsigned int start) ;
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);


};


class Nucleus
{
public:
	void init(const std::string dna_sequence);
	string get_RNA_transcript(const Gene& gene);
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

private:
	string _DNA_strand;
	string _complementary_DNA_strand;
};