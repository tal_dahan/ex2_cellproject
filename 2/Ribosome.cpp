#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include <iostream>

using namespace std;

/*
Function creat protein from RNA transcript
input:RNA_transcript
output:pointer to the ptotein list
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{	
	
	string copy = "";
	Protein  * protein = new Protein;
	protein->init();
	
	/*cheak if there is valid number of nucleus*/
	if (RNA_transcript.length() % 3 != 0)
	{
		return nullptr;
	}
	//creat the  list
	while (RNA_transcript.length() >= 3)
	{
		/*SAVE FIRST 3 CHARCATERS*/
		copy = RNA_transcript;
		copy.erase(3);
		/*delete the characters from the RNA*/
		RNA_transcript.erase(0,3);
		/*cheak if the acid is exist*/
		if (get_amino_acid(copy) == UNKNOWN)
		{
			/*clear the list memory*/
			protein->clear();
			//return nullptr if the acid is'nt exist
			return nullptr;
		}
		else
		{
			/*add the amino acid to the end of the list*/
			protein->add(get_amino_acid(copy));
		}
	}
	//return pointer to the list
	return protein;
}