#include "Cell.h"

using namespace std;

/*Function init all the varibles in the cell
input:dna_sequence,glucose_receptor_gene
output:non
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	/*init all the varibles*/
	this->_nucleus.init(dna_sequence);
	_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
}

bool  Cell::get_ATP()
{
	/*get the rna of the gene*/
	string RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	/*send the rna to the ribosome to get protein*/
	Protein *p = this->_ribosome.create_protein(RNA_transcript);
	/*cheak if the ribosome secceeded to create protein*/
	if (p==NULL)
	{
		// writes to cerr - a stream dedicated to error audit
		std::cerr << "problem" << endl;
		//exit 
		_exit(1);
	}
	/*send the protein to the mitochondrion*/
	this->_mitochondrion.insert_glucose_receptor(*p);
	/*set mitocgondrion glucos lvl ro 50*/
	this->_mitochondrion.set_glucose(50);
	/*cheak if the mitochondrion secceded to create atp*/
	if (this->_mitochondrion.produceATP(50) == false)
	{
		/*return false if he didn't*/
		return false;
	}
	/*change atp unints to 100*/
	this->_atp_units = 100;
	/*return true*/
	return true;

}