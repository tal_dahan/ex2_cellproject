﻿#include "Mitochondrion.h"
#include "Protein.h"



void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	string copy = "";
	Protein  nextP;
	Protein  p;
	p.init();
	
	bool isGood = true;

	/* create  protein configuration like we wanna compare to - ALANINE→LEUCINE→GLYCINE→HISTIDINE→LEUCINE→PHENYLALANINE→AMINO_CHAIN_END */
	string RNA_transcript = "GCACUAGGUCAUCUGUUCUAA";
	while (RNA_transcript.length() >= 3)
	{
		/*SAVE FIRST 3 CHARCATERS*/
		copy = RNA_transcript;
		copy.erase(3);
		/*delete the characters from the RNA*/
		RNA_transcript.erase(0, 3);
		/*add the amino acid to the end of the list*/
		p.add(get_amino_acid(copy));
		
	}
	nextP = protein;
	/*compare the proteins*/
	while (p.get_first()!=nullptr)
	{

		if (nextP.get_first() == nullptr)
		{
			isGood = false;
			break;
		}
		else
		{
			if (p.get_first()->get_data() != nextP.get_first()->get_data())
			{
				isGood = false;
				break;
			}
			p.set_first(p.get_first()->get_next());
			nextP.set_first(nextP.get_first()->get_next());
		}
	}
	if (isGood == true)
	{
		this->_has_glocuse_receptor= true;
	}

}

/*Function set new value to _glocuse_level
input:glocuse_units
output:non
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
Function check if the mitochondrion can create atp
input:glocuse_unit
output:true - if the  mitochondrion can create atp else false
*/
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	bool glocuse = false;
	//cheak if the glocuse level equle 50
	if (glocuse_unit == 50)
	{
		glocuse = true;
	}
	//return trueis the glocuse level equle 50  and there is glocuse receptor
	return(this->_has_glocuse_receptor && glocuse);
}